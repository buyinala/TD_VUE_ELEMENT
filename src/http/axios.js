// http.js
import axios from 'axios'
import QS from 'qs'
import Lockr from 'lockr'
import router from '../router'
import GLOBAL from '../config'

// 环境的切换
if (process.env.NODE_ENV === 'development') {
    // axios.defaults.baseURL = '/api'
} else if (process.env.NODE_ENV === 'production') {
    axios.defaults.baseURL = GLOBAL.domail
}
// 请求拦截器
axios.interceptors.request.use(config => {
    if(Lockr.get('token')){
        config.headers.common['token'] = Lockr.get('token')
    }
    return config
},
error => {
    return Promise.error(error)
})

axios.defaults.timeout = 500

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'
axios.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'
// sessionid 过期解决
axios.defaults.withCredentials = true

// 响应拦截器
axios.interceptors.response.use(response => {
    if (response.status === 200) {
        if (response.data.code === 405) {
            Lockr.rm('userInfo')
            Lockr.rm('uid')
            router.push({
                path: '/login',
                query: {
                    redirect: router.fullPath || '/'
                }
            })
            return Promise.resolve(response)
        } else if (response.data.code === 401) {
            response.data.msg = "暂无权限访问，快联系管理员吧"
            return Promise.resolve(response)
        } else if (response.data.code === 403) {
            response.data.msg = "禁止访问，快联系管理员吧"
            return Promise.resolve(response)
        } else if (response.data.code === 404) {
            response.data.msg = "找不到，我们正在努力哦"
            return Promise.resolve(response)
        } else if (response.data.code === 500) {
            response.data.msg = "网络不给力，请稍后重试哦"
            return Promise.resolve(response)
        } else {
            response.data.msg = "未知错误，请联系我们"
            return Promise.resolve(response)
        }
    } else {
        response.data.msg = "未知错误，请联系我们"
        return Promise.reject(response)
    }
}, error => {
    // 我们可以在这里对异常状态作统一处理
    let result = {}
    if (error && error.stack.indexOf('timeout') > -1) {
        result.msg = "请求超时，请稍后重试"
        return Promise.reject(result)
    }
    if (error && error.stack.indexOf('Network Error') > -1) {
        result.msg = "网络跑丢了，请检查网络"
        return Promise.reject(result)
    }
    if (error && error.response.status) {
        result.msg = "未知错误，请联系我们"
        return Promise.reject(result)
    }
})

let config = {
    headers: {
		'Content-Type': 'multipart/form-data;boundary = ' + new Date().getTime()
	}
};


// get 请求
export function httpGet(url, params = {}) {
    return new Promise((resolve, reject) => {
        axios.get(url, {
            params
        }).then((res) => {
            resolve(res.data)
        }).catch(err => {
            reject(err)
        })
    })
}

// post请求
export function httpPost(url, params={}) {
    return new Promise((resolve, reject) => {
        axios.post(url, QS.stringify(params))
            .then(res => {
                resolve(res.data);
            })
            .catch(err => {
                reject(err)
            })
    });
}

// put
export function httpPut(url, params={}) {
    let formData = new FormData();
    for (let i in params) {
        formData.append(i, params[i])
    }
    return new Promise((resolve, reject) => {
        axios.put(url, formData, config)
            .then(res => {
                resolve(res.data);
            })
            .catch(err => {
                reject(err)
            })
    });
}
// delete
export function httpDelete(url, params={}) {
    let formData = new FormData();
    for (let i in params) {
        formData.append(i, params[i])
    }
    return new Promise((resolve, reject) => {
        axios.delete(url, formData, config)
            .then(res => {
                resolve(res.data);
            })
            .catch(err => {
                reject(err)
            })
    });
}