import Vue from 'vue'
import ElementUI from 'element-ui'

import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/base.less'

import global from './config'
import animated from 'animate.css'
import store from './store'

import {httpPost, httpGet, httpPut, httpDelete} from "./http/axios";
import VueScroller from 'vue-scroller'

import VueClipboard from 'vue-clipboard2'
 
VueClipboard.config.autoSetContainer = true // add this line
Vue.use(VueClipboard)

Vue.use(VueScroller)

Vue.use(ElementUI)
Vue.use(animated)

Vue.prototype.GLOBAL = global
Vue.prototype.$post = httpPost
Vue.prototype.$get = httpGet
Vue.prototype.$put = httpPut
Vue.prototype.$delete = httpDelete

import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app')
