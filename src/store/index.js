import Vue from 'vue'
import Vuex from 'vuex'
import {httpGet} from '../http/axios'
import Lockr from 'lockr'
import GLOBAL from '@/config'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        userInfo: null,
    },
    mutations: {
        SET_USERINFO: (state, userInfo) => {
            state.userInfo = userInfo
        }
    },
    getters: {
        userInfo: state => state.userInfo,
    },
    actions: {
        // 登录
        Login({
            commit
        }, params) {
            return new Promise((resolve, reject) => {
                let url = '/api/user/login'
                httpGet(url, params).then(response => {
                    if (response.code === GLOBAL.SERVE_SUCCESS_CODE) {
                        const data = response.data.userinfo
                        Lockr.set('token', data.token)
                        Lockr.set('uid', data.user_id)
                        commit('SET_USERINFO', data)
                        resolve(response)
                    } else {
                        reject(response)
                    }
                }).catch(error => {
                    reject(error)
                })
            })
        }
    },
    //全局变量分组，提前声明其他store文件，然后引入这里
    modules: {}
})