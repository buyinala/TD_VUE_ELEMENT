export default {
    data() {
        return {
            // google verify
            verifyModel: {
                code: "",
            },
            verifyRules: {
                code: [
                    {
                        required: true,
                        message: this.$t("home.googel_code_p"),
                        trigger: "blur",
                    },
                ],
            },
            // password verify
            passwordModel: {
                password: "",
            },
            passwordRules: {
                password: [
                    {
                        required: true,
                        message: this.$t("home.login_password_p"),
                        trigger: "blur",
                    },
                ],
            },
        };
    },
    methods: {
        openVerification() {
            let that = this
            if (this.userInfo.goole_secret) {
                this.$refs.dialog.open({
                    title: that.$t("transfer.google_validate"),
                    showConfirmBtn: false,
                    showCancelBtn: false,
                });
            } else {
                this.$refs.dialog_password.open({
                    title: that.$t("transfer.password_validate"),
                    showConfirmBtn: false,
                    showCancelBtn: false,
                });
            }
        },
        // 验证密码
        async verifyPsw() {
            let params = {
                password: this.passwordModel.password,
            };
            let url = "/api/user/checkPwd";
            let result = await this.$post(url, params);
            if (result.code === this.GLOBAL.SERVE_SUCCESS_CODE) {
                this.$refs.dialog_password.close();
                this.passwordModel.password = ''
                this.requestForm();
            } else {
                this.disabledClick = false;
                this.Message({
                    message: result.msg,
                    type: "warning",
                });
            }
        },
        // 验证谷歌
        async verifGoogle() {
            let params = {
                oneCode: this.verifyModel.code,
            };
            let url = "/api/validate/check_google_secret";
            let result = await this.$post(url, params);
            if (result.code === this.GLOBAL.SERVE_SUCCESS_CODE) {
                this.disabledClick = false;
                this.verifyModel.code = ''
                this.$refs.dialog.close();
                this.requestForm();
            } else {
                this.disabledClick = false;
                this.Message({
                    message: result.msg,
                    type: "warning",
                });
            }
        },
    },
};
