/** request return code */
const serve = {
    SERVE_SUCCESS_CODE: 1,
    SERVE_FAIL_CODE: 0,
}
const domail = 'http://www.acsstudio.xyz'
const apiDomail = 'http://www.acsstudio.xyz'

export default {
    ...serve,
    domail,
    apiDomail
}