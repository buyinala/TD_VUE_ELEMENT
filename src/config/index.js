import serve from './modules/serve'
import Images from '@/assets'
export default {
    ...serve,
    ...Images
}