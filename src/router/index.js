import Vue from 'vue'
import Router from 'vue-router'
import Lockr from 'lockr'
import store from '../store'

Vue.use(Router)

const routers = [
    {
        path: '/502',
        component: () => import('@/views/502'),
        meta: {
            title: '服务器无响应',
        }
    },
    {
        path: '/404',
        component: () => import('@/views/404'),
        meta: {
            title: '找不到',
        }
    },
    {
        path: '*',
        component: () => import('@/views/404'),
        meta: {
            title: '找不到',
        }
    },
]

const RouterConfig = {
    // mode: 'history',
    scrollBehavior: () => ({
        y: 0
    }),
    routes: routers
}
const router = new Router(RouterConfig)
router.beforeEach((to, from, next) => {
    if (!Lockr.get('isFirst')&&to.path != '/guide') {
        next({
            path: '/guide',
        })
        return null
    }
    if (!to.meta.auth) {
        next()
    } else {
        if (!Lockr.get('token')) {
            next({
                path: '/login',
                query: {
                    redirect: to.fullPath || '/'
                }
            })
        } else {
            if (!store.getters.userInfo) {
                store.dispatch("UserInfo", {})
                    .then(res => {
                        next()
                    })
                    .catch((err) => {
                        console.log('userInfo-err=====>', err)
                    })
            } else {
                next()
            }
        }
    }
})

export default router
