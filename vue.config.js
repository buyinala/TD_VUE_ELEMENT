const path = require('path');

function resolve(dir) {
    return path.join(__dirname, dir);
}

module.exports = {
    //基本路径
    publicPath: './',
    //输出文件目录
    outputDir: 'dist',
    //是否在保存的时候检查
    lintOnSave: true,
    //放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录。
    assetsDir: 'index',
    devServer: {
        open: true, //是否自动弹出浏览器页面
        host: "0.0.0.0",
        port: '8080',
        https: false, //是否使用https协议
        hotOnly: true, //是否开启热更新
        // proxy: {
        //     '/api': {
        //         target: 'http://www.acsstudio.xyz', //API服务器的地址
        //         changeOrigin: true, // 虚拟的站点需要更管origin
        //         // pathRewrite: { //重写路径 比如'/api/aaa/ccc'重写为'/aaa/ccc'
        //         //     '^/api': ''
        //         // }
        //     }
        // },
    },
    // lintOnSave: true,
    chainWebpack: (config) => {
        config.resolve.alias
            .set('@', resolve('src'))
            .set('@assets', resolve('src/assets'))
        // 这里只写了两个个，你可以自己再加，按这种格式.set('', resolve(''))
    },
}